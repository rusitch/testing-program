﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using TestingProgram.Annotations;
using TestingProgram.Data;
using TestingProgram.ViewModels;

namespace TestingProgram.View
{
    public partial class TestSelectorWindow : INotifyPropertyChanged
    {
        private ObservableCollection<TestViewModel> _tests;
        private TestViewModel _selectedTest;

        public TestViewModel SelectedTest
        {
            get { return _selectedTest; }
            set
            {
                if (Equals(value, _selectedTest)) return;
                _selectedTest = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<TestViewModel> Tests
        {
            get { return _tests; }
            set
            {
                _tests = value;
                OnPropertyChanged(nameof(Tests));
            }
        }

        public TestSelectorWindow()
        {
            InitializeComponent();

            DataContext = this;

            var repository = new TestsRepository(new TestsContext());
            Tests = new ObservableCollection<TestViewModel>(repository.GetTests());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void TestsListBox_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (TestsListBox.SelectedItem != null)
            {
                SelectedTest = (TestViewModel) TestsListBox.SelectedItem;
                Close();
            }
        }
    }
}
