﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using TestingProgram.Annotations;
using TestingProgram.Data;
using TestingProgram.ViewModels;

namespace TestingProgram.View
{
    public partial class TestEditorWindow : INotifyPropertyChanged
    {
        private TestViewModel _currentTest;

        public TestViewModel CurrentTest {
            get { return _currentTest; }
            set
            {
                _currentTest = value;
                OnPropertyChanged(nameof(CurrentTest));
            }
        }

        public TestsRepository Repository { get; }

        public TestEditorWindow(TestViewModel testToEdit = null)
        {
            InitializeComponent();

            DataContext = this;

            Repository = new TestsRepository(new TestsContext());
            CurrentTest = testToEdit;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetupTest();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (!CloseCurrentTest())
                e.Cancel = true;
        }

        private void AddQuestionButton_Click(object sender, RoutedEventArgs e)
        {
            CurrentTest.Questions.Add(new QuestionViewModel {Text = "Empty"});

            var lastItem = CurrentTest.Questions.Count - 1;
            QuestionListBox.ScrollIntoView(CurrentTest.Questions[lastItem]);
            QuestionListBox.SelectedIndex = lastItem;

            QuestionTextBox.Focus();
            QuestionTextBox.SelectAll();
        }

        private void AddAnswerButton_Click(object sender, RoutedEventArgs e)
        {
            var answers = CurrentTest.Questions[QuestionListBox.SelectedIndex].Answers;
            answers.Add(new AnswerViewModel {Text = "Empty"});

            var lastItem = answers.Count - 1;
            AnswersListBox.ScrollIntoView(CurrentTest.Questions[QuestionListBox.SelectedIndex].Answers[lastItem]);
            AnswersListBox.SelectedIndex = lastItem;

            AnswerTextBox.Focus();
            AnswerTextBox.SelectAll();
        }

        private void RemoveQuestionButton_Click(object sender, RoutedEventArgs e)
        {
            int lastIndex = QuestionListBox.SelectedIndex;
            if (CurrentTest.Questions.Count > 0 && QuestionListBox.SelectedIndex != -1)
            {
                CurrentTest.Questions.RemoveAt(lastIndex);

                //Выбираем другой элемент в списке, чтобы было что-то выбрано после удаления
                if (CurrentTest.Questions.Count > 0) 
                {
                    if (CurrentTest.Questions.Count > lastIndex)
                        QuestionListBox.SelectedIndex = lastIndex;
                    else if (CurrentTest.Questions.Count == lastIndex)
                        QuestionListBox.SelectedIndex = lastIndex - 1;
                }
            }
        }

        private void RemoveAnswerButton_Click(object sender, RoutedEventArgs e)
        {
            var lastIndex = AnswersListBox.SelectedIndex;
            var answers = CurrentTest.Questions[QuestionListBox.SelectedIndex].Answers;

            if (answers.Count > 0 && AnswersListBox.SelectedIndex != -1)
            { 
                answers.RemoveAt(AnswersListBox.SelectedIndex);

                //Выбираем другой элемент в списке, чтобы было что-то выбрано после удаления
                if (answers.Count > 0) 
                {
                    if (answers.Count > lastIndex)
                        AnswersListBox.SelectedIndex = lastIndex;
                    else if (answers.Count == lastIndex)
                        AnswersListBox.SelectedIndex = lastIndex - 1;
                }
            }
        }


        //Происходит при выборе элемента в списке (ListBox) вопросов
        private void QuestionListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (QuestionListBox.SelectedIndex != -1)
            {
                RemoveQuestionButton.IsEnabled = true;
                CurrentQuestionGroupBox.IsEnabled = true;
                AnswersListGroupBox.IsEnabled = true;
            }
            else
            {
                RemoveQuestionButton.IsEnabled = false;
                CurrentQuestionGroupBox.IsEnabled = false;
                CurrentAnswerGroupBox.IsEnabled = false;
                AnswersListGroupBox.IsEnabled = false;
            }
        }

        private void AnswersListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AnswersListBox.SelectedIndex != -1)
            {
                CurrentAnswerGroupBox.IsEnabled = true;
                RemoveAnswerButton.IsEnabled = true;
            }
            else
            {
                CurrentAnswerGroupBox.IsEnabled = false;
                RemoveAnswerButton.IsEnabled = false;
            }
        }

        private void SetupTest()
        {
            CurrentTest = CurrentTest ?? new TestViewModel();

            QuestionListGroupBox.IsEnabled = true;
        }

        private void SaveCurrentTest()
        {
            Repository.SaveTest(CurrentTest);
        }

        private bool CloseCurrentTest()
        {
            if (true)
            {
                var testTitle = string.Empty;
                var messageText = "Хотите ли вы сохранить тест" + testTitle + "?";
                var result = MessageBox.Show(this, messageText, string.Empty, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.Cancel:
                        return false;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Yes:
                        SaveCurrentTest();
                        return true;
                }
            }
            CurrentTest = null;
            CurrentQuestionGroupBox.IsEnabled = false;
            QuestionListGroupBox.IsEnabled = false;
            CurrentAnswerGroupBox.IsEnabled = false;
            AnswersListGroupBox.IsEnabled = false;

            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
