﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TestingProgram.Annotations;
using TestingProgram.Data;
using TestingProgram.ViewModels;

namespace TestingProgram.View
{
    public partial class StatisticsWindow : INotifyPropertyChanged
    {
        private ObservableCollection<StatisticViewModel> _statistics;

        public ObservableCollection<StatisticViewModel> Statistics
        {
            get { return _statistics; }
            set
            {
                if (Equals(value, _statistics)) return;
                _statistics = value;
                OnPropertyChanged();
            }
        }

        public StatisticsWindow(int? studentId = null)
        {
            InitializeComponent();
            var repository = new TestsRepository(new TestsContext());

            DataContext = this;

            Statistics = new ObservableCollection<StatisticViewModel>(repository.GetStatistics(studentId));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
