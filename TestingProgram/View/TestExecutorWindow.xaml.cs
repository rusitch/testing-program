﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using TestingProgram.Data;
using TestingProgram.Logic;
using TestingProgram.ViewModels;

namespace TestingProgram.View
{
    public partial class TestExecutorWindow
    {
        private TestingLogic _testLogic;
        public ObservableCollection<AnswerViewModel> Answers { get; set; }
        private readonly TestViewModel _selectedTest;
        private readonly TestsRepository _repository;
        private readonly StudentViewModel _currentStudent;

        public TestExecutorWindow(TestViewModel selectedTest, StudentViewModel currentStudent)
        {
            InitializeComponent();
            _selectedTest = selectedTest;
            Answers = new ObservableCollection<AnswerViewModel>();
            _repository = new TestsRepository(new TestsContext());
            _currentStudent = currentStudent;
        }

        #region Обработчики событий TestingLogic

        private void TestLogic_TestSelected(object sender, EventArgs e)
        {
            _testLogic.NextQuestion();
        }

        private void TestLogic_NextQuestionSelected(object sender, QuestionEventArgs e)
        {
            ShowQuestion(e.Question);
        }

        private void TestLogic_TestEnded(object sender, EventArgs e)
        {
            var tLogic = (TestingLogic)sender;

            _repository.SaveStatistic(new StatisticViewModel
            {
                RightAnswersCount = tLogic.CorrectResponses,
                TotalAnswersCount = tLogic.QuantityQuestions,
                StudentId = _currentStudent.Id,
                TestId = _selectedTest.Id,
                CreatedDate = DateTime.Now
            });

            ShowResults(tLogic.CorrectResponses, tLogic.QuantityQuestions);
            Close();
        }
        #endregion

        #region Обработчики событий окна и контролов MainForm

        private void MainForm_Shown(object sender, EventArgs e)
        {
            LbAnswers.ItemsSource = Answers;
            _testLogic = new TestingLogic(_selectedTest);
            _testLogic.NextQuestionSelected += TestLogic_NextQuestionSelected;
            _testLogic.TestEnded += TestLogic_TestEnded;
            _testLogic.TestSelected += TestLogic_TestSelected;
            _testLogic.NextQuestion();
        }

        private void NextQuestionButton_Click(object sender, EventArgs e)
        {
            _testLogic.AcceptAnswers(Answers.Where(x => x.IsSelected));
            _testLogic.NextQuestion();
        }

        #endregion

        #region Методы

        private void ShowQuestion(QuestionViewModel question)
        {
            QuestionLabel.Content = question.Text;
            Answers.Clear();
            question.Answers.ToList().ForEach(x => Answers.Add(x));
        }

        private void ShowResults(int correctResponses, int quantityQuestions)
        {
            string text = $"Правильных ответов: {correctResponses} из {quantityQuestions} вопросов";
            MessageBox.Show(this, text, null);
        }

        #endregion
    }
}
