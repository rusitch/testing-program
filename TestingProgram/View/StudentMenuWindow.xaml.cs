﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using TestingProgram.Annotations;
using TestingProgram.ViewModels;

namespace TestingProgram.View
{
    public partial class StudentMenuWindow : INotifyPropertyChanged
    {
        private StudentViewModel _currenStudent;

        public StudentViewModel CurrenStudent   
        {
            get { return _currenStudent; }
            set
            {
                if (Equals(value, _currenStudent)) return;
                _currenStudent = value;
                OnPropertyChanged();
            }
        }

        public StudentMenuWindow(StudentViewModel student)
        {
            InitializeComponent();
            CurrenStudent = student;
        }

       

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void StartTestButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var window = new TestSelectorWindow();
            window.ShowDialog();
            if (window.SelectedTest != null)
            {
                new TestExecutorWindow(window.SelectedTest, CurrenStudent).ShowDialog();
            }
        }

        private void StatisticsButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var window = new StatisticsWindow(CurrenStudent.Id);
            window.ShowDialog();
        }
    }
}
