﻿using TestingProgram.Data;

namespace TestingProgram.View
{
    public partial class TeacherMenuWindow
    {
        private readonly TestsRepository _repository;

        public TeacherMenuWindow()
        {
            InitializeComponent();
            _repository = new TestsRepository(new TestsContext());
        }

        private void AddTestButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var window = new TestEditorWindow();
            window.ShowDialog();
        }

        private void EditTestButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var window = new TestSelectorWindow();
            window.ShowDialog();
            if (window.SelectedTest != null)
            {
                new TestEditorWindow(window.SelectedTest).ShowDialog();
            }
        }

        private void RemoveTestButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var window = new TestSelectorWindow();
            window.ShowDialog();
            if (window.SelectedTest != null)
            {
                _repository.RemoveTest(window.SelectedTest.Id);
            }
        }

        private void Statistics_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var window = new StatisticsWindow();
            window.ShowDialog();
        }
    }
}
