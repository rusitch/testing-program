﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using TestingProgram.Annotations;
using TestingProgram.Data;
using TestingProgram.ViewModels;

namespace TestingProgram.View
{
    public partial class LoginWindow : INotifyPropertyChanged
    {
        public TeacherViewModel CurrentTeacher
        {
            get { return _currentTeacher; }
            set
            {
                if (Equals(value, _currentTeacher)) return;
                _currentTeacher = value;
                OnPropertyChanged();
            }
        }

        public StudentViewModel CurrentStudent
        {
            get { return _currentStudent; }
            set
            {
                if (Equals(value, _currentStudent)) return;
                _currentStudent = value;
                OnPropertyChanged();
            }
        }

        private readonly TestsRepository _repository;
        private TeacherViewModel _currentTeacher = new TeacherViewModel();
        private StudentViewModel _currentStudent = new StudentViewModel();

        public LoginWindow()
        {
            InitializeComponent();
            DataContext = this;
            _repository = new TestsRepository(new TestsContext());
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (TabControl.SelectedIndex != 0) // Вчитель
            {
                if (_repository
                    .GetTeachers()
                    .Any(x => x.Login == CurrentTeacher.Login && x.Password == PasswordBox.Password))
                {
                    var window = new TeacherMenuWindow();
                    window.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Вчителя не знайдено!");
                }
            }
            else
            {
                var existingStudent = _repository
                    .GetStudents()
                    .FirstOrDefault(
                        x =>
                            x.Class == CurrentStudent.Class && 
                            x.FirstName == CurrentStudent.FirstName &&
                            x.LastName == CurrentStudent.LastName);

                var student = existingStudent ?? _repository.AddStudent(CurrentStudent);

                var window = new StudentMenuWindow(student);
                window.ShowDialog();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
