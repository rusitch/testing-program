﻿using System.Data.Entity;
using TestingProgram.Models;

namespace TestingProgram.Data
{
    public class TestsContext : DbContext
    {
        public TestsContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Test> Tests { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Statistic> Statistics { get; set; }

        public void Seed(TestsContext context)
        {
            var defaultTeacher = new Teacher
            {
                Login = "teacher",
                Password = "123456"
            };
            context.Teachers.Add(defaultTeacher);

            context.SaveChanges();
        }

        public class DropCreateIfChangeInitializer : DropCreateDatabaseIfModelChanges<TestsContext>
        {
            protected override void Seed(TestsContext context)
            {
                context.Seed(context);

                base.Seed(context);
            }
        }

        public class CreateInitializer : CreateDatabaseIfNotExists<TestsContext>
        {
            protected override void Seed(TestsContext context)
            {
                context.Seed(context);

                base.Seed(context);
            }
        }

        static TestsContext()
        {
            Database.SetInitializer(new CreateInitializer());
        }
    }
}
