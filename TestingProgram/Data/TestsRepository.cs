﻿using System.Collections.Generic;
using System.Linq;
using TestingProgram.ViewModels;

namespace TestingProgram.Data
{
    public class TestsRepository
    {
        private readonly TestsContext _context;

        public TestsRepository(TestsContext context)
        {
            _context = context;
        }

        public void SaveTest(TestViewModel test)
        {
            var model = test.ToModel();

            if (_context.Tests.Any(x => x.Id == model.Id))
            {
                var existingTest = _context.Tests.Single(x => x.Id == model.Id);
                _context.Entry(existingTest).CurrentValues.SetValues(model);

                foreach (var existingQuestion in existingTest.Questions.ToList())
                    if (model.Questions.All(s => s.Id != existingQuestion.Id))
                        _context.Questions.Remove(existingQuestion);

                foreach (var newQuestion in model.Questions)
                {
                    var existingQuestion = existingTest.Questions.SingleOrDefault(s => s.Id == newQuestion.Id);
                    if (existingQuestion != null)
                    {
                        // Update that are in collection
                        _context.Entry(existingQuestion).CurrentValues.SetValues(newQuestion);

                        foreach (var existingAnswer in existingQuestion.Answers.ToList())
                            if (newQuestion.Answers.All(s => s.Id != existingAnswer.Id))
                                _context.Answers.Remove(existingAnswer);

                        foreach (var newAnswer in newQuestion.Answers)
                        {
                            var existingAnswer = existingQuestion.Answers.SingleOrDefault(s => s.Id == newAnswer.Id);
                            if (existingAnswer != null)
                            {
                                // Update that are in collection
                                _context.Entry(existingAnswer).CurrentValues.SetValues(newAnswer);
                            }
                            else
                                existingQuestion.Answers.Add(newAnswer);
                        }
                    }
                    else
                        existingTest.Questions.Add(newQuestion);
                }

                _context.SaveChanges();
            }
            else
            {
                _context.Tests.Add(model);
            }

            _context.SaveChanges();
        }

        public TestViewModel GetTest(int testId)
        {
            return _context.Tests.Single(x => x.Id == testId).ToView();
        }

        public List<TestViewModel> GetTests()
        {
            return _context.Tests.ToList().Select(x => x.ToView()).ToList();
        }

        public List<StatisticViewModel> GetStatistics(int? studentId = null)
        {
            return studentId.HasValue 
                ? _context.Statistics.Where(x => x.StudentId == studentId).ToList().Select(x => x.ToView()).ToList() 
                : _context.Statistics.ToList().Select(x => x.ToView()).ToList();
        }

        public List<TeacherViewModel> GetTeachers()
        {
            return _context.Teachers.ToList().Select(x => x.ToView()).ToList();
        }

        public List<StudentViewModel> GetStudents()
        {
            return _context.Students.ToList().Select(x => x.ToView()).ToList();
        }

        public StudentViewModel AddStudent(StudentViewModel student)
        {
            var addedStudent = _context.Students.Add(student.ToModel());
            _context.SaveChanges();

            return addedStudent.ToView();
        }

        public void SaveStatistic(StatisticViewModel statisticViewModel)
        {
            _context.Statistics.Add(statisticViewModel.ToModel());
            _context.SaveChanges();
        }

        public void RemoveTest(int id)
        {
            var testToRemove = _context.Tests.Single(x => x.Id == id);
            _context.Tests.Remove(testToRemove);
            _context.SaveChanges();
        }
    }
}
