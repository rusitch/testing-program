﻿using System.Collections.Generic;
using TestingProgram.ViewModels;

namespace TestingProgram.Logic
{
    public class AnswersEqualComparer : IEqualityComparer<AnswerViewModel>
    {
        public bool Equals(AnswerViewModel x, AnswerViewModel y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(AnswerViewModel obj)
        {
            return obj.Id;
        }
    }
}
