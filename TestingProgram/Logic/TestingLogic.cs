﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestingProgram.ViewModels;

namespace TestingProgram.Logic
{
    public class TestingLogic
    {
        protected QuestionViewModel CurrentQuestion;
        protected TestViewModel Test;
        protected int CurrentQuestionIndex = -1;

        public TestingLogic(TestViewModel test)
        {
            if (test == null)
                throw new ArgumentNullException(nameof(test));
            Test = test;
        }

        #region События

        /// <summary>Происходит, при выборе следущего вопроса</summary>
        public event EventHandler<QuestionEventArgs> NextQuestionSelected;
        /// <summary>Происходит, когда в тесте закончились вопросы</summary>
        public event EventHandler TestEnded;
        /// <summary>Происходит, когда выбран следующий тест</summary>
        public event EventHandler TestSelected;

        #endregion

        #region Методы инициирующие события

        public void OnNextQuestionSelected(QuestionViewModel question)
        {
            var handler = NextQuestionSelected;
            handler?.Invoke(this, new QuestionEventArgs(question));
        }

        public void OnTestEnded()
        {
            var handler = TestEnded;
            handler?.Invoke(this, EventArgs.Empty);
        }

        public void OnTestSelected()
        {
            var handler = TestSelected;
            handler?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        #region Свойства
        /// <summary>Количество вопросов в текущем тесте</summary>
        public int QuantityQuestions => Test.Questions.Count;

        /// <summary>Количество правильных ответов в текущем тесте</summary>
        public int CorrectResponses { get; protected set; }
        #endregion

        #region public Методы

        public virtual void ChooseTest(TestViewModel test)
        {
            if (test == null)
                throw new ArgumentNullException(nameof(test));
            Test = test;
            ResetStatsOfCurrentTest();
            OnTestSelected();
        }

        /// <summary>
        /// Перейти к следующему вопросу, в случае, если вопросы закончились, будет инициировано событие TestEnded.
        /// </summary>
        public virtual void NextQuestion()
        {
            if (++CurrentQuestionIndex < QuantityQuestions)
            {
                CurrentQuestion = Test.Questions[CurrentQuestionIndex];
                OnNextQuestionSelected(CurrentQuestion);
            }
            else
            {
                OnTestEnded();
            }
        }

        public virtual void AcceptAnswers(IEnumerable<AnswerViewModel> answers)
        {
            if (answers == null)
                throw new ArgumentNullException(nameof(answers));

            if (CurrentQuestion.Answers.Where(x => x.IsRightAnswer).SequenceEqual(answers, new AnswersEqualComparer()))
            {
                CorrectResponses++;
            }
        }

        /// <summary>
        /// Сбросить статистику для текущего теста
        /// </summary>
        public virtual void ResetStatsOfCurrentTest()
        {
            CorrectResponses = 0;
        }

        #endregion

    }
}
