﻿using System;
using TestingProgram.ViewModels;

namespace TestingProgram.Logic
{
    public class QuestionEventArgs : EventArgs
    {
        public QuestionEventArgs(QuestionViewModel question)
        {
            Question = question;
        }

        public QuestionViewModel Question { get; protected set; }
    }
}
