﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestingProgram.Models
{
    public class Statistic
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int TestId { get; set; }

        public int StudentId { get; set; }

        public int RightAnswersCount { get; set; }

        public int TotalAnswersCount { get; set; }

        public DateTime CreatedDate { get; set; }

        public virtual Test Test { get; set; }

        public virtual Student Student { get; set; }
    }
}
