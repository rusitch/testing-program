﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestingProgram.Models
{
    public class Question
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int TestId { get; set; }

        public string Text { get; set; }

        public virtual IList<Answer> Answers { get; set; }

        public virtual Test Test { get; set; }
    }
}
