﻿using System.Collections.ObjectModel;
using System.Linq;
using TestingProgram.Models;

namespace TestingProgram.ViewModels
{
    internal static class Mapper
    {
        public static TestViewModel ToView(this Test test)
        {
            return new TestViewModel
            {
                Id = test.Id,
                Title = test.Title,
                Description = test.Description,
                Questions = new ObservableCollection<QuestionViewModel>(test.Questions.Select(x => x.ToView()))
            };
        }

        public static QuestionViewModel ToView(this Question question)
        {
            return new QuestionViewModel
            {
                Id = question.Id,
                Text = question.Text,
                TestId = question.TestId,
                Answers = new ObservableCollection<AnswerViewModel>(question.Answers.Select(x => x.ToView()))
            };
        }

        public static AnswerViewModel ToView(this Answer answer)
        {
            return new AnswerViewModel
            {
                Id = answer.Id,
                Text = answer.Text,
                IsRightAnswer = answer.IsRightAnswer,
                QuestionId = answer.QuestionId
            };
        }

        public static TeacherViewModel ToView(this Teacher teacher)
        {
            return new TeacherViewModel
            {
                Id = teacher.Id,
                Login = teacher.Login,
                Password = teacher.Password
            };
        }

        public static StudentViewModel ToView(this Student student)
        {
            return new StudentViewModel
            {
                Id = student.Id,
                Class = student.Class,
                FirstName = student.FirstName,
                LastName = student.LastName
            };
        }

        public static StatisticViewModel ToView(this Statistic statistic)
        {
            return new StatisticViewModel
            {
                Id = statistic.Id,
                RightAnswersCount = statistic.RightAnswersCount,
                StudentId = statistic.StudentId,
                TestId = statistic.TestId,
                TotalAnswersCount = statistic.TotalAnswersCount,
                CreatedDate = statistic.CreatedDate,
                Answers = $"{statistic.RightAnswersCount}/{statistic.TotalAnswersCount}",
                StudentName = $"{statistic.Student.LastName} {statistic.Student.FirstName}",
                TestName = statistic.Test.Title
            };
        }

        public static Test ToModel(this TestViewModel test)
        {
            return new Test
            {
                Id = test.Id,
                Title = test.Title,
                Description = test.Description,
                Questions = test.Questions.Select(x => x.ToModel()).ToList()
            };
        }

        public static Question ToModel(this QuestionViewModel question)
        {
            return new Question
            {
                Id = question.Id,
                Text = question.Text,
                TestId = question.TestId,
                Answers = question.Answers.Select(x => x.ToModel()).ToList()
            };
        }

        public static Answer ToModel(this AnswerViewModel answer)
        {
            return new Answer
            {
                Id = answer.Id,
                Text = answer.Text,
                IsRightAnswer = answer.IsRightAnswer,
                QuestionId = answer.QuestionId
            };
        }

        public static Teacher ToModel(this TeacherViewModel teacher)
        {
            if (teacher == null)
                return null;

            return new Teacher
            {
                Id = teacher.Id,
                Login = teacher.Login,
                Password = teacher.Password
            };
        }

        public static Student ToModel(this StudentViewModel student)
        {
            if (student == null)
                return null;

            return new Student
            {
                Id = student.Id,
                Class = student.Class,
                FirstName = student.FirstName,
                LastName = student.LastName
            };
        }

        public static Statistic ToModel(this StatisticViewModel statistic)
        {
            return new Statistic
            {
                Id = statistic.Id,
                RightAnswersCount = statistic.RightAnswersCount,
                StudentId = statistic.StudentId,
                TestId = statistic.TestId,
                TotalAnswersCount = statistic.TotalAnswersCount,
                CreatedDate = statistic.CreatedDate
            };
        }
    }
}
