﻿using System;
using System.ComponentModel;

namespace TestingProgram.ViewModels
{
    [Serializable]
    public class AnswerViewModel : INotifyPropertyChanged
    {
        private string _text;
        private bool _isRightAnswer;
        private bool _isSelected;
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; OnPropertyChanged("Text"); }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; OnPropertyChanged("IsSelected"); }
        }

        public bool IsRightAnswer
        { 
            get { return _isRightAnswer; }
            set { _isRightAnswer = value; OnPropertyChanged("IsRightAnswer"); }
        }

        public int QuestionId { get; set; }

        /// <summary>Уведомляет подписчика о изменении свойства</summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
