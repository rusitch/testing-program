﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace TestingProgram.ViewModels
{
    public class TestViewModel : INotifyPropertyChanged
    {
        private int _id;
        private string _title;
        private string _description;
        private ObservableCollection<QuestionViewModel> _questions;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }
        public string Title
        {
            get { return _title; }
            set { _title = value; OnPropertyChanged("Title"); }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; OnPropertyChanged("Description"); }
        }

        public virtual ObservableCollection<QuestionViewModel> Questions
        {
            get { return _questions; }
            set { _questions = value; OnPropertyChanged("Questions"); }
        }

        public TestViewModel()
        {
            _questions = new ObservableCollection<QuestionViewModel>();
        }

        /// <summary>Уведомляет подписчика о изменении свойства</summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
