﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TestingProgram.Annotations;

namespace TestingProgram.ViewModels
{
    public class StatisticViewModel : INotifyPropertyChanged
    {
        private int _totalAnswersCount;
        private int _rightAnswersCount;
        private int _studentId;
        private int _testId;
        private int _id;

        public int Id
        {
            get { return _id; }
            set
            {
                if (value == _id) return;
                _id = value;
                OnPropertyChanged();
            }
        }

        public int TestId
        {
            get { return _testId; }
            set
            {
                if (value == _testId) return;
                _testId = value;
                OnPropertyChanged();
            }
        }

        public int StudentId
        {
            get { return _studentId; }
            set
            {
                if (value == _studentId) return;
                _studentId = value;
                OnPropertyChanged();
            }
        }

        public int RightAnswersCount
        {
            get { return _rightAnswersCount; }
            set
            {
                if (value == _rightAnswersCount) return;
                _rightAnswersCount = value;
                OnPropertyChanged();
            }
        }

        public int TotalAnswersCount
        {
            get { return _totalAnswersCount; }
            set
            {
                if (value == _totalAnswersCount) return;
                _totalAnswersCount = value;
                OnPropertyChanged();
            }
        }

        public DateTime CreatedDate { get; set; }

        public string StudentName { get; set; }

        public string TestName { get; set; }

        public string Answers { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
