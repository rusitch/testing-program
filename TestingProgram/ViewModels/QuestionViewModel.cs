﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace TestingProgram.ViewModels
{
    public class QuestionViewModel : INotifyPropertyChanged
    {
        private string _text;
        private ObservableCollection<AnswerViewModel> _answers;
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged("Id"); }
        }
        public string Text
        {
            get { return _text; }
            set { _text = value; OnPropertyChanged("Text"); }
        }

        public int TestId { get; set; }

        public ObservableCollection<AnswerViewModel> Answers
        {
            get { return _answers; }
            set { _answers = value; OnPropertyChanged("Answers"); }
        }

        public QuestionViewModel()
        {
            _answers = new ObservableCollection<AnswerViewModel>();    
        }

        /// <summary>Уведомляет подписчика о изменении свойства</summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
